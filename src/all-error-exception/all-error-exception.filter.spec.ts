import { AllErrorExceptionFilter } from './all-error-exception.filter';

describe('AllErrorExceptionFilter', () => {
  it('should be defined', () => {
    expect(new AllErrorExceptionFilter()).toBeDefined();
  });
});
