import { ForbiddenErrorExceptionFilter } from './forbidden-error-exception.filter';

describe('ForbiddenErrorExceptionFilter', () => {
  it('should be defined', () => {
    expect(new ForbiddenErrorExceptionFilter()).toBeDefined();
  });
});
