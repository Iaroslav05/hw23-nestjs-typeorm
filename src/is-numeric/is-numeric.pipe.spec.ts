import { IsNumericPipe } from './is-numeric.pipe';

describe('IsNumericPipe', () => {
  it('should be defined', () => {
    expect(new IsNumericPipe()).toBeDefined();
  });
});
