import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { LoggerService } from './logger.service';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  constructor(private readonly loggerService: LoggerService) {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const requestId = uuidv4();
    const req = context.switchToHttp().getRequest();
    const date = new Date().toLocaleString('uk-UA');

    this.loggerService.log('New request coming:', {
      requestId,
      date,
      url: context.switchToHttp().getRequest().url,
      method: context.switchToHttp().getRequest().method,
      body: req.body,
      query: req.query,
      params: req.params,
    });

    return next.handle().pipe(
      tap((body) => {
        this.loggerService.log('Response completed', {
          requestId,
          date,
          url: context.switchToHttp().getRequest().url,
          method: context.switchToHttp().getRequest().method,
          body,
          query: req.query,
          params: req.params,
        });
      }),
    );
  }
}
