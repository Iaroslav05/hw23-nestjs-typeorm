import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { RecordService } from './record.service';
import { CreateRecordDto } from './dto/create-record.dto';
import { UpdateRecordDto } from './dto/update-record.dto';
import { ApiKeyGuard } from './api-key.guard';
import { IsNumericPipe } from '../is-numeric/is-numeric.pipe';

@Controller('record')
@UseGuards(ApiKeyGuard)
export class RecordController {
  constructor(private readonly recordService: RecordService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  create(@Body() createRecordDto: CreateRecordDto) {
    return this.recordService.create(createRecordDto);
  }

  @Get()
  findAll() {
    return this.recordService.findAll();
  }

  @Get(':id')
  findOneEncoded(@Param('id', IsNumericPipe) id: string) {
    return this.recordService.findOneEncoded(+id);
  }

  @Get(':id/decoded')
  findOneDecoded(@Param('id', IsNumericPipe) id: string) {
    return this.recordService.findOneDecoded(+id);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  update(
    @Param('id', IsNumericPipe) id: string,
    @Body() updateRecordDto: UpdateRecordDto,
  ) {
    return this.recordService.update(+id, updateRecordDto);
  }

  @Delete(':id')
  remove(@Param('id', IsNumericPipe) id: string) {
    return this.recordService.remove(+id);
  }
}
